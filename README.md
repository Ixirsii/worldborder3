# WorldBorder3 #

A rewrite of [WorldBorder 2](https://github.com/PryPurity/WorldBorder) in 
Kotlin with more modern coding conventions and *hopefully* fewer memory leaks. 
