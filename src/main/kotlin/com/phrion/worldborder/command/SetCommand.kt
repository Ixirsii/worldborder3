/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.command

import com.google.inject.Inject
import com.phrion.worldborder.data.ConfigSchema
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.FileConfiguration
import org.slf4j.Logger
import javax.inject.Singleton

/**
 * [WorldBorderCommand] for /worldborder set.
 *
 * Sets a configuration value.
 *
 * @since 2021-12-12
 */
@Singleton
class SetCommand @Inject constructor(
    override val log: Logger,
    private val config: FileConfiguration
) : WorldBorderCommand {
    override val description: String = "Update plugin configuration. Requires reload for changes to take effect."
    override val usage: String = """
        /<command> set <flag> <value>
        flag    - The configuration value to set as defined in config.yml.
        value   - The value to set
    """.trimIndent()

    /**
     * Set a configuration value.
     *
     * @param sender Source of the command.
     * @param command Command which was executed.
     * @param label Alias of the command which was used.
     * @param args Passed command arguments.
     * @return `true` if a valid command, otherwise `false`.
     */
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        log.trace("{} called {} {}", sender, label, args)

        if (!verifyArguments(args, 2, 2, sender)) {
            return false
        }

        when (val flag: String = args[0]) {
            ConfigSchema.AUTOSAVE_FREQUENCY.flag -> setIntConfiguration(
                flag,
                ConfigSchema.AUTOSAVE_FREQUENCY.key,
                args[1],
                sender
            )
            ConfigSchema.CHECK_RATE_IN_TICKS.flag -> setIntConfiguration(
                flag,
                ConfigSchema.CHECK_RATE_IN_TICKS.key,
                args[1],
                sender
            )
            ConfigSchema.DENY_ENDER_PEARLS.flag -> setBooleanConfiguration(
                flag,
                ConfigSchema.DENY_ENDER_PEARLS.key,
                args[1],
                sender
            )
            ConfigSchema.KILL_PLAYER.flag -> setBooleanConfiguration(
                flag,
                ConfigSchema.KILL_PLAYER.key,
                args[1],
                sender
            )
            ConfigSchema.KNOCK_BACK.flag -> setIntConfiguration(
                flag,
                ConfigSchema.KNOCK_BACK.key,
                args[1],
                sender
            )
            ConfigSchema.MESSAGE.flag -> setConfiguration(flag, ConfigSchema.MESSAGE.key, args[1], sender)
            ConfigSchema.ROUND_BORDER.flag -> setBooleanConfiguration(
                flag,
                ConfigSchema.ROUND_BORDER.key,
                args[1],
                sender
            )
            ConfigSchema.TASK_MEMORY_USAGE_TOLERANCE.flag -> setIntConfiguration(
                flag,
                ConfigSchema.TASK_MEMORY_USAGE_TOLERANCE.key,
                args[1],
                sender
            )
            ConfigSchema.WHOOSH_EFFECT.flag -> setBooleanConfiguration(
                flag,
                ConfigSchema.WHOOSH_EFFECT.key,
                args[1],
                sender
            )
            else -> {
                sender.sendMessage("Invalid configuration key: $flag")

                return false
            }
        }

        return true
    }

    /* ********************************************************************************************************** *
     *                                          Private utility methods                                           *
     * ********************************************************************************************************** */

    /**
     * Set a configuration value to a boolean value.
     *
     * @param flag The human-readable version of the configuration key.
     * @param key The full YAML configuration key.
     * @param valueString The string argument passed in by [sender].
     * @param sender Source of the command.
     */
    private fun setBooleanConfiguration(flag: String, key: String, valueString: String, sender: CommandSender) {
        val value: Boolean

        try {
            value = valueString.toBooleanStrict()
        } catch (e: IllegalArgumentException) {
            sender.sendMessage("\"$valueString\" is not \"true\" or \"false\"")

            return
        }

        setConfiguration(flag, key, value, sender)
    }

    /**
     * Set a configuration value to an integer value.
     *
     * @param flag The human-readable version of the configuration key.
     * @param key The full YAML configuration key.
     * @param valueString The string argument passed in by [sender].
     * @param sender Source of the command.
     */
    private fun setIntConfiguration(flag: String, key: String, valueString: String, sender: CommandSender) {
        val value: Int

        try {
            value = valueString.toInt()
        } catch (e: NumberFormatException) {
            sender.sendMessage("\"$valueString\" is not a valid number")

            return
        }

        setConfiguration(flag, key, value, sender)
    }

    /**
     *
     * @param flag The human-readable version of the configuration key.
     * @param key The full YAML configuration key.
     * @param value The parsed configuration value.
     * @param sender Source of the command.
     */
    private fun setConfiguration(flag: String, key: String, value: Any, sender: CommandSender) {
        log.debug("{} set configuration {} to {}", sender, key, value)
        config.set(key, value)
        sender.sendMessage("$flag set to $value. You will have to reload the plugin for this change to take effect")
    }
}
