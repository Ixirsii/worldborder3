/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.command

import com.google.inject.Inject
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.slf4j.Logger
import javax.inject.Singleton

/**
 * [WorldBorderCommand] for /worldborder.
 *
 * Parses arguments and calls the appropriate sub-command.
 *
 * @since 2021-12-12
 */
@Singleton
class BaseCommand @Inject constructor(
    override val log: Logger,
    private val commands: Map<String, WorldBorderCommand>
) : WorldBorderCommand {
    override val description: String = "Limit the size of your worlds."
    override val usage: String = "/<command> <subcommand>"

    /**
     * Parse the subcommand and delegate to the appropriate executor.
     *
     * @param sender Source of the command.
     * @param command Command which was executed.
     * @param label Alias of the command which was used.
     * @param args Passed command arguments.
     * @return `true` if a valid command, otherwise `false`.
     */
    override fun onCommand(sender: CommandSender, command: Command, label: String, args: Array<out String>): Boolean {
        log.trace("{} called {} {}", sender, label, args)

        if (args.isEmpty()) {
            return false
        }

        val flag: String = args[0]
        val subCommand: WorldBorderCommand? = commands[flag]

        if (subCommand != null) {
            if (!subCommand.onCommand(sender, command, flag, args.drop(1).toTypedArray())) {
                sender.sendMessage("Usage: ${subCommand.usage}")
            }
        } else {
            sender.sendMessage("Unrecognized command \"$flag\"")

            return false
        }

        return true
    }
}
