/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.command

import com.phrion.worldborder.Logging
import org.bukkit.command.CommandExecutor
import org.bukkit.command.CommandSender

/**
 * Plugin command executor.
 *
 * @since 2021-12-12
 */
interface WorldBorderCommand: CommandExecutor, Logging {
    /**
     * Command description.
     */
    val description: String

    /**
     * Command usage.
     */
    val usage: String

    /**
     * Verify arguments passed to command.
     *
     * @param args Arguments passed to command.
     * @param minimum Minimum number of arguments accepted.
     * @param maximum Maximum number of arguments accepted (if there is a maximum). Setting this to a value less than
     * 0 (IE. -1) will skip this check.
     * @param sender Command sender for sending error messages.
     * @return `true` if a valid number of arguments was passed, otherwise `false`.
     */
    fun verifyArguments(args: Array<out String>, minimum: Int, maximum: Int, sender: CommandSender): Boolean {
        return if (args.size < minimum) {
            sender.sendMessage("Too few arguments provided")

            false
        } else if ((maximum > -1) && (args.size > maximum)) {
            sender.sendMessage("Unrecognized arguments: ${args.drop(maximum)}")

            false
        } else {
            true
        }
    }
}
