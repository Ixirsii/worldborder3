/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.data

/**
 * Default border configuration object.
 */
private const val BORDER = "border"

/**
 * Database configuration object.
 */
private const val DATABASE = "database"

/**
 * Dynmap configuration object.
 */
private const val DYNMAP = "dynmap"

/**
 * Flat file database configuration object.
 */
private const val FLAT_FILE = "$DATABASE.flatfile"

/**
 * MongoDB database configuration object.
 */
private const val MONGODB = "$DATABASE.mongodb"

/**
 * MySQL database configuration object.
 */
private const val MYSQL = "$DATABASE.mysql"

/**
 * SQLite database configuration object.
 */
private const val SQLITE = "$DATABASE.sqlite"

/**
 * Default task configuration object.
 */
private const val TASK = "task"

/**
 * Configuration (config.yml) schema.
 *
 * @since 2021-12-12
 */
enum class ConfigSchema(val key: String, val flag: String = key) {
    /**
     * Default task autosave frequency configuration.
     */
    AUTOSAVE_FREQUENCY("$TASK.autosaveFrequency", "autosaveFrequency"),

    /**
     * Default behavior when a player crosses a border.
     */
    BORDER_MODE("$BORDER.mode", "borderMode"),

    /**
     * Default rate (in ticks) that the plugin will check for players crossing the border.
     */
    CHECK_RATE_IN_TICKS("checkRateInTicks"),

    /**
     * Default border deny ender pearl configuration.
     */
    DENY_ENDER_PEARLS("$BORDER.denyEnderPearl", "denyEnderPearl"),

    /**
     * Is Dynmap integration enabled?
     */
    DYNMAP_ENABLED("$DYNMAP.enabled", "dynmapEnabled"),

    /**
     * Dynmap world border label.
     */
    DYNMAP_LABEL("$DYNMAP.label", "dynmapLabel"),

    /**
     * Use flat file (YAML) database.
     */
    FLAT_FILE_ENABLED("$FLAT_FILE.enabled", "flatfile"),

    /**
     * Default border kill player configuration.
     */
    KILL_PLAYER("$BORDER.killPlayer", "killPlayer"),

    /**
     * Default border knock back configuration.
     */
    KNOCK_BACK("$BORDER.knockBack", "knockBack"),

    /**
     * Default border message configuration.
     */
    MESSAGE("$BORDER.message", "message"),

    /**
     * MongoDB database name.
     */
    MONGODB_DATABASE("$MONGODB.database", "mongodbDatabase"),

    /**
     * Use MongoDB database.
     */
    MONGODB_ENABLED("$MONGODB.enabled", "mongodb"),

    /**
     * MongoDB database hostname/URL.
     */
    MONGODB_HOSTNAME("$MONGODB.hostname", "mongodbHostname"),

    /**
     * MongoDB database hostname/URL.
     */
    MONGODB_OPTIONS("$MONGODB.options", "mongodbOptions"),

    /**
     * MongoDB database password.
     */
    MONGODB_PASSWORD("$MONGODB.password", "mongodbPassword"),

    /**
     * MongoDB database port.
     */
    MONGODB_PORT("$MONGODB.port", "mongodbPort"),

    /**
     * MongoDB collection prefix.
     */
    MONGODB_PREFIX("$MONGODB.collectionPrefix", "mongodbPrefix"),

    /**
     * MongoDB database username.
     */
    MONGODB_USERNAME("$MONGODB.username", "mongodbUsername"),

    /**
     * MySQL database name.
     */
    MYSQL_DATABASE("$MYSQL.database", "mysqlDatabase"),

    /**
     * Use MySQL database.
     */
    MYSQL_ENABLED("$MYSQL.enabled", "mysql"),

    /**
     * MySQL database hostname/URL.
     */
    MYSQL_HOSTNAME("$MYSQL.hostname", "mysqlHostname"),

    /**
     * MySQL connection flags.
     */
    MYSQL_OPTIONS("$MYSQL.options", "mysqlOptions"),

    /**
     * MySQL database password.
     */
    MYSQL_PASSWORD("$MYSQL.password", "mysqlPassword"),

    /**
     * MySQL database port.
     */
    MYSQL_PORT("$MYSQL.port", "mysqlPort"),

    /**
     * MySQL table prefix.
     */
    MYSQL_PREFIX("$MYSQL.tablePrefix", "mysqlPrefix"),

    /**
     * MySQL database username.
     */
    MYSQL_USERNAME("$MYSQL.username", "mysqlUsername"),

    /**
     * Default border shape configuration.
     */
    ROUND_BORDER("$BORDER.roundBorder", "roundBorder"),

    /**
     * Use MySQL database.
     */
    SQLITE_ENABLED("$SQLITE.enabled", "sqlite"),

    /**
     * Task memory usage tolerance.
     */
    TASK_MEMORY_USAGE_TOLERANCE("taskMemoryUsageTolerance"),

    /**
     * Default border whoosh effect configuration.
     */
    WHOOSH_EFFECT("$BORDER.whooshEffect", "whooshEffect")
}
