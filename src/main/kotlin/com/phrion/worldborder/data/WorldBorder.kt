/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.data

import com.phrion.worldborder.repository.BORDER_MODE
import com.phrion.worldborder.repository.DENY_ENDER_PEARL
import com.phrion.worldborder.repository.KNOCK_BACK
import com.phrion.worldborder.repository.MESSAGE
import com.phrion.worldborder.repository.RADIUS_X
import com.phrion.worldborder.repository.RADIUS_Z
import com.phrion.worldborder.repository.ROUND_BORDER
import com.phrion.worldborder.repository.WHOOSH_EFFECT
import com.phrion.worldborder.repository.WORLD
import com.phrion.worldborder.repository.X
import com.phrion.worldborder.repository.Z
import lombok.EqualsAndHashCode
import lombok.ToString
import org.bson.codecs.pojo.annotations.BsonCreator
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.codecs.pojo.annotations.BsonProperty

/**
 * World border database object.
 *
 * @since 2012-12-12
 */
@EqualsAndHashCode
@ToString
data class WorldBorder @BsonCreator constructor(
    /** Behavior when a player crosses the border. */
    @BsonProperty(BORDER_MODE)
    val borderMode: BorderMode,
    /** If `true` cancels ender pearls, otherwise adjusts the target location. */
    @BsonProperty(DENY_ENDER_PEARL)
    val denyEnderPearl: Boolean,
    /** How far to move the player back inside the border when they cross it. */
    @BsonProperty(KNOCK_BACK)
    val knockBack: Double,
    /** Message that players see when they cross a border. */
    @BsonProperty(MESSAGE)
    val message: String,
    /** x-axis radius of the border. */
    @BsonProperty(RADIUS_X)
    val radiusX: Int,
    /** z-axis radius of the border. */
    @BsonProperty(RADIUS_Z)
    val radiusZ: Int,
    /** If `true` the border is round, otherwise the border is square. */
    @BsonProperty(ROUND_BORDER)
    val roundBorder: Boolean,
    /** Does crossing the border play a "whoosh" sound effect. */
    @BsonProperty(WHOOSH_EFFECT)
    val whooshEffect: Boolean,
    /** World name. */
    @BsonId
    @BsonProperty(WORLD)
    val world: String,
    /** Center x-coordinate. */
    @BsonProperty(X)
    val x: Double,
    /** Center z-coordinate. */
    @BsonProperty(Z)
    val z: Double
)
