package com.phrion.worldborder.data

/**
 * Behavior when a player crosses a border.
 *
 * @since 2021-12-17
 */
enum class BorderMode {
    /** Kill the player. */
    KILL,

    /** Knock the player back inside the border (default). */
    KNOCK_BACK,

    /** Teleport the player to the opposite side of the world. */
    WRAP
}