/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.data

import lombok.EqualsAndHashCode
import lombok.ToString

/**
 * Plugin configuration settings.
 *
 * @since 2012-12-12
 */
@EqualsAndHashCode
@ToString
data class Config(
    /**
     * How often (in server ticks) to check for players crossing world borders.
     */
    val checkRateInTicks: Int,
    /**
     * Database configuration.
     */
    val databaseConfig: DatabaseConfig,
    /**
     * Default task auto save frequency used if none is specified.
     * @see Task.autoSaveFrequency
     */
    val defaultAutoSaveFrequency: Int,
    /**
     * Default behavior when a player crosses the border if none is specified.
     * @see WorldBorder.borderMode
     */
    val defaultBorderMode: BorderMode,
    /**
     * Default deny ender pearl setting if none is specified.
     * @see WorldBorder.denyEnderPearl
     */
    val defaultDenyEnderPearl: Boolean,
    /**
     * Default border knock back setting if none is specified.
     * @see WorldBorder.knockBack
     */
    val defaultKnockBack: Double,
    /**
     * Default border message if none is specified.
     * @see WorldBorder.message
     */
    val defaultMessage: String,
    /**
     * Default border shape if none is specified.
     * @see WorldBorder.roundBorder
     */
    val defaultRoundBorder: Boolean,
    /**
     * Default whoosh effect setting if none is specified.
     * @see WorldBorder.whooshEffect
     */
    val defaultWhooshEffect: Boolean,
    /**
     * Is Dynmap integration enabled?
     */
    val dynmapEnabled: Boolean,
    /**
     * Dynmap border label if integration is enabled.
     */
    val dynmapLabel: String,
    /**
     * The memory threshold below which running tasks will automatically pause until memory is freed up.
     */
    val taskMemoryUsageTolerance: Int
)
