/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.repository

import com.phrion.worldborder.Logging
import com.phrion.worldborder.data.WorldBorder
import java.util.Optional
import com.mongodb.MongoException
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.Filters.eq
import com.mongodb.client.model.Updates.combine
import com.mongodb.client.model.Updates.set
import com.mongodb.client.result.DeleteResult
import com.mongodb.client.result.InsertOneResult
import com.mongodb.client.result.UpdateResult
import org.slf4j.Logger

/**
 * World border MongoDB repository.
 *
 * @since 2021-12-18
 */
class WorldBorderMongoRepository(
    /** World border MongoDB collection. */
    private val collection: MongoCollection<WorldBorder>,
    /** SLF4J plugin logger. */
    override val log: Logger
) : Logging, WorldBorderRepository {

    /**
     * Insert a world border to the collection.
     *
     * @param worldBorder World border to insert.
     * @return `true` if border was successfully inserted, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     */
    @Throws(MongoException::class)
    override fun createBorder(worldBorder: WorldBorder): Boolean {
        log.trace("Inserting world border {} to Mongo collection", worldBorder)

        val result: InsertOneResult = collection.insertOne(worldBorder)

        if (!result.wasAcknowledged()) {
            log.error("Failed to insert border into Mongo collection: {}", worldBorder)

            return false
        }

        return true
    }

    /**
     * Remove a world border from the database.
     *
     * @param world World name.
     * @return `true` if border was successfully deleted, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     */
    @Throws(MongoException::class)
    override fun deleteBorder(world: String): Boolean {
        log.trace("Deleting border for world {} from database", world)

        val result: DeleteResult = collection.deleteMany(eq(WORLD, world))

        if (!result.wasAcknowledged()) {
            log.error("Failed to delete border for world {}", world)

            return false
        }

        return true
    }

    /**
     * Get border for a world.
     *
     * @param world World name.
     * @return [WorldBorder] for world if it exists, otherwise [Optional.empty].
     * @throws MongoException on Mongo Java Driver error.
     */
    @Throws(MongoException::class)
    override fun readBorder(world: String): Optional<WorldBorder> {
        log.trace("Getting world border {} from Mongo collection", world)

        return Optional.ofNullable(collection.find(eq(WORLD, world)).first())
    }

    /**
     * List all world borders.
     *
     * @return all world borders.
     * @throws MongoException on Mongo Java Driver error.
     */
    @Throws(MongoException::class)
    override fun readBorders(): List<WorldBorder> {
        log.trace("Reading all borders from Mongo collection")

        return collection.find().into(mutableListOf())
    }

    /**
     * Update world border center point.
     *
     * @param world World name.
     * @param x Center point x-coordinate.
     * @param z Center point z-coordinate.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.x
     * @see WorldBorder.z
     */
    @Throws(MongoException::class)
    override fun updateCenter(world: String, x: Double, z: Double): Boolean {
        log.trace("Updating center value to ({}, {}) for world {}", x, z, world)

        val result: UpdateResult = collection.updateOne(
            eq(WORLD, world),
            combine(set(X, x), set(Z, z)))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update center point coordinates for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border `denyEnderPearl` value.
     *
     * @param world World name.
     * @param denyEnderPearl `denyEnderPearl` value.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.denyEnderPearl
     */
    @Throws(MongoException::class)
    override fun updateDenyEnderPearl(world: String, denyEnderPearl: Boolean): Boolean {
        log.trace("Updating denyEnderPearl value to {} for world {} in Mongo collection", denyEnderPearl, world)

        val result: UpdateResult = collection.updateOne(eq(WORLD, world), set(DENY_ENDER_PEARL, denyEnderPearl))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update denyEnderPearl value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border knock back value.
     *
     * @param world World name.
     * @param knockBack Distance to knock players back behind border.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.knockBack
     */
    @Throws(MongoException::class)
    override fun updateKnockBack(world: String, knockBack: Double): Boolean {
        log.trace("Updating knockBack value to {} for world {}", knockBack, world)

        val result: UpdateResult = collection.updateOne(eq(WORLD, world), set(KNOCK_BACK, knockBack))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update knockBack value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border message.
     *
     * @param world World name.
     * @param message Message shown to players who cross the border.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.message
     */
    @Throws(MongoException::class)
    override fun updateMessage(world: String, message: String): Boolean {
        log.trace("Updating message value to {} for world {}", message, world)

        val result: UpdateResult = collection.updateOne(eq(WORLD, world), set(MESSAGE, message))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update message value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border radius.
     *
     * @param world World name.
     * @param radiusX Border radius along the x-axis.
     * @param radiusZ Border radius along the z-axis.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.radiusX
     * @see WorldBorder.radiusZ
     */
    @Throws(MongoException::class)
    override fun updateRadius(world: String, radiusX: Int, radiusZ: Int): Boolean {
        log.trace("Updating radius value to ({}, {}) for world {}", radiusX, radiusZ, world)

        val result: UpdateResult = collection.updateOne(
            eq(WORLD, world),
            combine(set(RADIUS_X, radiusX), set(RADIUS_Z, radiusZ)))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update radius value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border shape.
     *
     * @param world World name.
     * @param roundBorder Border shape.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.roundBorder
     */
    @Throws(MongoException::class)
    override fun updateRoundBorder(world: String, roundBorder: Boolean): Boolean {
        log.trace("Updating roundBorder value to {} for world {}", roundBorder, world)

        val result: UpdateResult = collection.updateOne(eq(WORLD, world), set(ROUND_BORDER, roundBorder))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update roundBorder value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }

    /**
     * Update border whoosh effect value.
     *
     * @param world World name.
     * @param whooshEffect Does crossing the border play a "whoosh" sound effect.
     * @return `true` if value was successfully updated, or `false` on failure.
     * @throws MongoException on Mongo Java Driver error.
     * @see WorldBorder.whooshEffect
     */
    @Throws(MongoException::class)
    override fun updateWhooshEffect(world: String, whooshEffect: Boolean): Boolean {
        log.trace("Updating whooshEffect value to {} for world {}", whooshEffect, world)

        val result: UpdateResult = collection.updateOne(eq(WORLD, world), set(WHOOSH_EFFECT, whooshEffect))

        if (!result.wasAcknowledged() || result.modifiedCount < 1) {
            log.error("Failed to update whooshEffect value for world {} in Mongo collection. {}", world, result)

            return false
        }

        return true
    }
}
