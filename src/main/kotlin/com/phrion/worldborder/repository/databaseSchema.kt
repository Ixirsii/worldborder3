/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.repository

/**
 * Border mode column name.
 */
internal const val BORDER_MODE = "borderMode"

/**
 * Table name for world borders.
 */
internal const val BORDER_TABLE = "borders"

/**
 * Deny Ender pearl column name.
 */
internal const val DENY_ENDER_PEARL = "denyEnderPearl"

/**
 * Knock back column name.
 */
internal const val KNOCK_BACK = "knockBack"

/**
 * Border message column name.
 */
internal const val MESSAGE = "message"

/**
 * Player ID column name.
 */
internal const val PLAYER_ID = "playerId"

/**
 * X radius column name.
 */
internal const val RADIUS_X = "radiusX"

/**
 * Z radius column name.
 */
internal const val RADIUS_Z = "radiusZ"

/**
 * Border shape column name.
 */
internal const val ROUND_BORDER = "roundBorder"

/**
 * Whoosh effect column name.
 */
internal const val WHOOSH_EFFECT = "whooshEffect"

/**
 * World name column name.
 */
internal const val WORLD = "world"

/**
 * Center x-coordinate column name.
 */
internal const val X = "x"

/**
 * Center z-coordinate column name.
 */
internal const val Z = "z"
