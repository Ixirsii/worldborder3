/******************************************************************************
 * Copyright (c) 2022, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.repository

import com.phrion.worldborder.Logging
import com.phrion.worldborder.data.WorldBorder
import com.phrion.worldborder.data.BorderMode
import java.io.File
import java.io.IOException
import java.util.Optional
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton
import org.bukkit.configuration.file.FileConfiguration
import org.slf4j.Logger

/**
 * World border YAML file repository.
 *
 * @since 2022-01-01
 */
@Singleton
class WorldBorderFileRepository @Inject constructor(
    /** YAML storage file wrapper. */
    @Named("worldBorderFileConfiguration")
    private val configuration: FileConfiguration,
    /** Storage file. */
    @Named("worldBorderFile")
    private val file: File,
    /** SLF4J plugin logger. */
    override val log: Logger,
) : Logging, WorldBorderRepository {

    /**
     * Add a world border to the database.
     *
     * @param worldBorder World border to persist to database.
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun createBorder(worldBorder: WorldBorder): Boolean {
        log.trace("Saving border {} to file", worldBorder)

        return if (configuration.contains(worldBorder.world)) {
            false
        } else {
            configuration.set("${worldBorder.world}.$BORDER_MODE", worldBorder.borderMode.name)
            configuration.set("${worldBorder.world}.$DENY_ENDER_PEARL", worldBorder.denyEnderPearl)
            configuration.set("${worldBorder.world}.$KNOCK_BACK", worldBorder.knockBack)
            configuration.set("${worldBorder.world}.$MESSAGE", worldBorder.message)
            configuration.set("${worldBorder.world}.$RADIUS_X", worldBorder.radiusX)
            configuration.set("${worldBorder.world}.$RADIUS_Z", worldBorder.radiusZ)
            configuration.set("${worldBorder.world}.$ROUND_BORDER", worldBorder.roundBorder)
            configuration.set("${worldBorder.world}.$WHOOSH_EFFECT", worldBorder.whooshEffect)
            configuration.set("${worldBorder.world}.$X", worldBorder.x)
            configuration.set("${worldBorder.world}.$Z", worldBorder.z)
            configuration.save(file)

            true
        }
    }

    /**
     * Delete a world border from the database.
     *
     * @param world World to delete border from.
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun deleteBorder(world: String): Boolean {
        log.trace("Deleting border for world {} from file", world)

        return if (configuration.contains(world)) {
            configuration.set(world, null)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Read world border from the database.
     *
     * @param world World border to read.
     * @return Optional containing the [WorldBorder] if it exists, otherwise [Optional.empty].
     */
    override fun readBorder(world: String): Optional<WorldBorder> {
        log.trace("Reading border for world {} from file", world)

        return if (configuration.contains(world)) {
            Optional.of(buildBorder(world))
        } else {
            Optional.empty()
        }
    }

    /**
     * Read all world borders from the database.
     *
     * @return all world borders.
     */
    override fun readBorders(): List<WorldBorder> {
        log.trace("Reading all world borders from file")

        return configuration.getKeys(false).map(this::buildBorder)
    }

    /**
     * Update world border center point.
     *
     * @param world World name.
     * @param x Center point x-coordinate.
     * @param z Center point z-coordinate.
     * @see WorldBorder.x
     * @see WorldBorder.z
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateCenter(world: String, x: Double, z: Double): Boolean {
        log.trace("Updating center value to ({}, {}) for world {}", x, z, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$X", x)
            configuration.set("$world.$Z", z)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border `denyEnderPearl` value.
     *
     * @param world World name.
     * @param denyEnderPearl `denyEnderPearl` value.
     * @see WorldBorder.denyEnderPearl
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateDenyEnderPearl(world: String, denyEnderPearl: Boolean): Boolean {
        log.trace("Updating denyEnderPearl value to {} for world {}", denyEnderPearl, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$DENY_ENDER_PEARL", denyEnderPearl)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border knock back value.
     *
     * @param world World name.
     * @param knockBack Distance to knock players back behind border.
     * @see WorldBorder.knockBack
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateKnockBack(world: String, knockBack: Double): Boolean {
        log.trace("Updating knockBack value to {} for world {}", knockBack, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$KNOCK_BACK", knockBack)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border message.
     *
     * @param world World name.
     * @param message Message shown to players who cross the border.
     * @see WorldBorder.message
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateMessage(world: String, message: String): Boolean {
        log.trace("Updating message value to {} for world {}", message, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$MESSAGE", message)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border radius.
     *
     * @param world World name.
     * @param radiusX Border radius along the x-axis.
     * @param radiusZ Border radius along the z-axis.
     * @see WorldBorder.radiusX
     * @see WorldBorder.radiusZ
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateRadius(world: String, radiusX: Int, radiusZ: Int): Boolean {
        log.trace("Updating radius value to ({}, {}) for world {}", radiusX, radiusZ, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$RADIUS_X", radiusX)
            configuration.set("$world.$RADIUS_Z", radiusZ)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border shape.
     *
     * @param world World name.
     * @param roundBorder Border shape.
     * @see WorldBorder.roundBorder
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateRoundBorder(world: String, roundBorder: Boolean): Boolean {
        log.trace("Updating roundBorder value to {} for world {}", roundBorder, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$ROUND_BORDER", roundBorder)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /**
     * Update border whoosh effect value.
     *
     * @param world World name.
     * @param whooshEffect Does crossing the border play a "whoosh" sound effect.
     * @see WorldBorder.whooshEffect
     * @throws IOException on failure to write changes to disk.
     */
    @Throws(IOException::class)
    override fun updateWhooshEffect(world: String, whooshEffect: Boolean): Boolean {
        log.trace("Updating whooshEffect value to {} for world {}", whooshEffect, world)

        return if (configuration.contains(world)) {
            configuration.set("$world.$DENY_ENDER_PEARL", whooshEffect)
            configuration.save(file)

            true
        } else {
            false
        }
    }

    /* ********************************************************************************************************** *
     *                                          Private utility methods                                           *
     * ********************************************************************************************************** */

    private fun buildBorder(world: String): WorldBorder {
        return WorldBorder(
            borderMode = BorderMode.valueOf(configuration.getString("$world.$BORDER_MODE")!!),
            denyEnderPearl = configuration.getBoolean("$world.$DENY_ENDER_PEARL"),
            knockBack = configuration.getDouble("$world.$KNOCK_BACK"),
            message = configuration.getString("$world.$MESSAGE")!!,
            radiusX = configuration.getInt("$world.$RADIUS_X"),
            radiusZ = configuration.getInt("$world.$RADIUS_Z"),
            roundBorder = configuration.getBoolean("$world.$ROUND_BORDER"),
            world = world,
            whooshEffect = configuration.getBoolean("$world.$WHOOSH_EFFECT"),
            x = configuration.getDouble("$world.$X"),
            z = configuration.getDouble("$world.$Z")
        )
    }
}
