/******************************************************************************
 * Copyright (c) 2022, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.repository

import com.phrion.worldborder.data.WorldBorder
import java.io.IOException
import java.sql.SQLException
import java.util.Optional
import com.mongodb.MongoException

/**
 * Definition for repositories which persist [WorldBorder]s to disk.
 *
 * @since 2022-01-01
 */
interface WorldBorderRepository {

    /**
     * Add a world border to the database.
     *
     * @param worldBorder World border to persist to database.
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun createBorder(worldBorder: WorldBorder): Boolean

    /**
     * Delete a world border from the database.
     *
     * @param world World to delete border from.
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun deleteBorder(world: String): Boolean

    /**
     * Read world border from the database.
     *
     * @param world World border to read.
     * @return Optional containing the [WorldBorder] if it exists, otherwise [Optional.empty].
     */
    fun readBorder(world: String): Optional<WorldBorder>

    /**
     * Read all world borders from the database.
     *
     * @return all world borders.
     */
    fun readBorders(): List<WorldBorder>

    /**
     * Update world border center point.
     *
     * @param world World name.
     * @param x Center point x-coordinate.
     * @param z Center point z-coordinate.
     * @see WorldBorder.x
     * @see WorldBorder.z
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateCenter(world: String, x: Double, z: Double): Boolean

    /**
     * Update border `denyEnderPearl` value.
     *
     * @param world World name.
     * @param denyEnderPearl `denyEnderPearl` value.
     * @see WorldBorder.denyEnderPearl
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateDenyEnderPearl(world: String, denyEnderPearl: Boolean): Boolean

    /**
     * Update border knock back value.
     *
     * @param world World name.
     * @param knockBack Distance to knock players back behind border.
     * @see WorldBorder.knockBack
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateKnockBack(world: String, knockBack: Double): Boolean

    /**
     * Update border message.
     *
     * @param world World name.
     * @param message Message shown to players who cross the border.
     * @see WorldBorder.message
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateMessage(world: String, message: String): Boolean

    /**
     * Update border radius.
     *
     * @param world World name.
     * @param radiusX Border radius along the x-axis.
     * @param radiusZ Border radius along the z-axis.
     * @see WorldBorder.radiusX
     * @see WorldBorder.radiusZ
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateRadius(world: String, radiusX: Int, radiusZ: Int): Boolean

    /**
     * Update border shape.
     *
     * @param world World name.
     * @param roundBorder Border shape.
     * @see WorldBorder.roundBorder
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateRoundBorder(world: String, roundBorder: Boolean): Boolean

    /**
     * Update border whoosh effect value.
     *
     * @param world World name.
     * @param whooshEffect Does crossing the border play a "whoosh" sound effect.
     * @see WorldBorder.whooshEffect
     */
    @Throws(IOException::class, MongoException::class, SQLException::class)
    fun updateWhooshEffect(world: String, whooshEffect: Boolean): Boolean
}
