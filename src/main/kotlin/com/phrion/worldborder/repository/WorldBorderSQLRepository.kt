/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.repository

import com.phrion.worldborder.Logging
import com.phrion.worldborder.data.WorldBorder
import com.phrion.worldborder.data.BorderMode
import org.slf4j.Logger
import java.sql.Connection
import java.sql.ResultSet
import java.sql.SQLException
import java.util.*
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Generic world border SQL repository.
 *
 * @since 2021-12-16
 */
@Singleton
class WorldBorderSQLRepository @Inject constructor(
    /** Database connection. */
    private val connection: Connection,
    /** SLF4J plugin logger. */
    override val log: Logger,
    /** Table prefix. */
    @Named("tablePrefix")
    tablePrefix: String
) : Logging, WorldBorderRepository {

    /** World border able name with prefix. */
    private val table: String

    init {
        table = "$tablePrefix$BORDER_TABLE"

        createTable()
    }

    /**
     * Add a world border to the database.
     *
     * @param worldBorder World border to persist to database.
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun createBorder(worldBorder: WorldBorder): Boolean {
        log.trace("Saving border {} to database", worldBorder)

        connection.prepareStatement(
            """
            INSERT INTO $table(
                $WORLD,
                $X,
                $Z,
                $RADIUS_X,
                $RADIUS_Z,
                $ROUND_BORDER,
                $BORDER_MODE,
                $DENY_ENDER_PEARL,
                $KNOCK_BACK,
                $MESSAGE,
                $WHOOSH_EFFECT
            ) VALUES (
                ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?
            );""".trimIndent()
        )
            .use {
                it.setString(1, worldBorder.world)
                it.setDouble(2, worldBorder.x)
                it.setDouble(3, worldBorder.z)
                it.setInt(4, worldBorder.radiusX)
                it.setInt(5, worldBorder.radiusZ)
                it.setBoolean(6, worldBorder.roundBorder)
                it.setString(7, worldBorder.borderMode.name)
                it.setBoolean(8, worldBorder.denyEnderPearl)
                it.setDouble(9, worldBorder.knockBack)
                it.setString(10, worldBorder.message)
                it.setBoolean(11, worldBorder.whooshEffect)
                it.execute()
            }

        return true
    }

    /**
     * Remove a world border from the database.
     *
     * @param world World name.
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun deleteBorder(world: String): Boolean {
        log.trace("Deleting border for world {} from database", world)

        connection.prepareStatement("DELETE FROM $table WHERE $WORLD = ?;")
            .use {
                it.setString(1, world)
                it.execute()
            }

        return true
    }

    /**
     * Get border for a world.
     *
     * @param world World name.
     * @return [WorldBorder] for world if it exists, otherwise [Optional.empty].
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun readBorder(world: String): Optional<WorldBorder> {
        log.trace("Reading border for world {} from database", world)

        connection.prepareStatement("SELECT * FROM $table WHERE $WORLD = ?;")
            .use {
                it.setString(1, world)
                it.executeQuery().use { resultSet ->
                    if (resultSet.next()) {
                        return Optional.of(buildBorder(resultSet))
                    }
                }
            }

        return Optional.empty()
    }

    /**
     * List all world borders.
     *
     * @return all world borders.
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun readBorders(): List<WorldBorder> {
        log.trace("Reading all borders from database")

        val worldBorders: MutableList<WorldBorder> = mutableListOf()

        connection.prepareStatement("SELECT * FROM $table;")
            .use {
                it.executeQuery().use { resultSet ->
                    if (resultSet.next()) {
                        worldBorders.add(buildBorder(resultSet))
                    }
                }
            }

        return worldBorders
    }

    /**
     * Update world border center point.
     *
     * @param world World name.
     * @param x Center point x-coordinate.
     * @param z Center point z-coordinate.
     * @see WorldBorder.x
     * @see WorldBorder.z
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateCenter(world: String, x: Double, z: Double): Boolean {
        log.trace("Updating center value to ({}, {}) for world {}", x, z, world)

        connection.prepareStatement("UPDATE $table SET $X = ?, $Z = ? WHERE $WORLD = ?;")
            .use {
                it.setDouble(1, x)
                it.setDouble(2, z)
                it.setString(3, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border `denyEnderPearl` value.
     *
     * @param world World name.
     * @param denyEnderPearl `denyEnderPearl` value.
     * @see WorldBorder.denyEnderPearl
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateDenyEnderPearl(world: String, denyEnderPearl: Boolean): Boolean {
        log.trace("Updating denyEnderPearl value to {} for world {}", denyEnderPearl, world)

        connection.prepareStatement("UPDATE $table SET $DENY_ENDER_PEARL = ? WHERE $WORLD = ?;")
            .use {
                it.setBoolean(1, denyEnderPearl)
                it.setString(2, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border knock back value.
     *
     * @param world World name.
     * @param knockBack Distance to knock players back behind border.
     * @see WorldBorder.knockBack
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateKnockBack(world: String, knockBack: Double): Boolean {
        log.trace("Updating knockBack value to {} for world {}", knockBack, world)

        connection.prepareStatement("UPDATE $table SET $KNOCK_BACK = ? WHERE $WORLD = ?;")
            .use {
                it.setDouble(1, knockBack)
                it.setString(2, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border message.
     *
     * @param world World name.
     * @param message Message shown to players who cross the border.
     * @see WorldBorder.message
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateMessage(world: String, message: String): Boolean {
        log.trace("Updating message value to {} for world {}", message, world)

        connection.prepareStatement("UPDATE $table SET $MESSAGE = ? WHERE $WORLD = ?;")
            .use {
                it.setString(1, message)
                it.setString(2, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border radius.
     *
     * @param world World name.
     * @param radiusX Border radius along the x-axis.
     * @param radiusZ Border radius along the z-axis.
     * @see WorldBorder.radiusX
     * @see WorldBorder.radiusZ
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateRadius(world: String, radiusX: Int, radiusZ: Int): Boolean {
        log.trace("Updating radius value to ({}, {}) for world {}", radiusX, radiusZ, world)

        connection.prepareStatement("UPDATE $table SET $RADIUS_X = ?, $RADIUS_Z = ? WHERE $WORLD = ?;")
            .use {
                it.setInt(1, radiusX)
                it.setInt(2, radiusZ)
                it.setString(3, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border shape.
     *
     * @param world World name.
     * @param roundBorder Border shape.
     * @see WorldBorder.roundBorder
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateRoundBorder(world: String, roundBorder: Boolean): Boolean {
        log.trace("Updating roundBorder value to {} for world {}", roundBorder, world)

        connection.prepareStatement("UPDATE $table SET $ROUND_BORDER = ? WHERE $WORLD = ?;")
            .use {
                it.setBoolean(1, roundBorder)
                it.setString(2, world)
                it.execute()
            }

        return true
    }

    /**
     * Update border whoosh effect value.
     *
     * @param world World name.
     * @param whooshEffect Does crossing the border play a "whoosh" sound effect.
     * @see WorldBorder.whooshEffect
     * @throws SQLException on JDBC error.
     */
    @Throws(SQLException::class)
    override fun updateWhooshEffect(world: String, whooshEffect: Boolean): Boolean {
        log.trace("Updating whooshEffect value to {} for world {}", whooshEffect, world)

        connection.prepareStatement("UPDATE $table SET $WHOOSH_EFFECT = ? WHERE $WORLD = ?;")
            .use {
                it.setBoolean(1, whooshEffect)
                it.setString(2, world)
                it.execute()
            }

        return true
    }

    /* ********************************************************************************************************** *
     *                                          Private utility methods                                           *
     * ********************************************************************************************************** */

    /**
     * Create the world border table.
     */
    @Throws(SQLException::class)
    private fun createTable() {
        log.debug("Creating world border table")

        connection.prepareStatement(
            """CREATE TABLE IF NOT EXISTS $table(
                $WORLD TEXT PRIMARY KEY NOT NULL,
                $X INT NOT NULL,
                $Z INT NOT NULL,
                $RADIUS_X INT NOT NULL,
                $RADIUS_Z INT NOT NULL,
                $ROUND_BORDER INT(1) NOT NULL,
                $BORDER_MODE TEXT NOT NULL,
                $DENY_ENDER_PEARL INT(1) NOT NULL,
                $KNOCK_BACK DOUBLE NOT NULL,
                $MESSAGE TEXT NOT NULL,
                $WHOOSH_EFFECT INT(1) NOT NULL
            );""".trimIndent()
        )
            .use { it.execute() }
    }

    /**
     * Convert a result set into a [WorldBorder].
     *
     * @param resultSet Query results.
     * @return [WorldBorder] from query results.
     */
    private fun buildBorder(resultSet: ResultSet): WorldBorder {
        return WorldBorder(
            borderMode = BorderMode.valueOf(resultSet.getString(BORDER_MODE)),
            denyEnderPearl = resultSet.getBoolean(DENY_ENDER_PEARL),
            knockBack = resultSet.getDouble(KNOCK_BACK),
            message = resultSet.getString(MESSAGE),
            radiusX = resultSet.getInt(RADIUS_X),
            radiusZ = resultSet.getInt(RADIUS_Z),
            roundBorder = resultSet.getBoolean(ROUND_BORDER),
            whooshEffect = resultSet.getBoolean(WHOOSH_EFFECT),
            world = resultSet.getString(WORLD),
            x = resultSet.getDouble(X),
            z = resultSet.getDouble(Z)
        )
    }
}
