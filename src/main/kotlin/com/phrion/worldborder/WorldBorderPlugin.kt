/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder

import com.google.inject.Guice
import com.google.inject.Injector
import com.phrion.worldborder.command.BaseCommand
import com.phrion.worldborder.data.Config
import com.phrion.worldborder.module.ConfigModule
import com.phrion.worldborder.module.DatabaseModule
import com.phrion.worldborder.module.WorldBorderModule
import org.bukkit.plugin.java.JavaPlugin

/**
 * Main plugin class.
 *
 * @since 2021-12-12
 */
class WorldBorderPlugin: JavaPlugin() {
    private val injector: Injector

    init {
        val configModule = ConfigModule(slF4JLogger, this)
        val configInjector: Injector = Guice.createInjector(configModule)
        val config: Config = configInjector.getInstance(Config::class.java)

        injector = Guice.createInjector(
            configModule,
            DatabaseModule(config),
            WorldBorderModule(slF4JLogger, this)
        )
    }

    override fun onDisable() {
    }

    override fun onEnable() {
        // Copy default config.yml to plugin directory if it does not exist.
        saveDefaultConfig()
        registerCommandExecutors()
    }

    /* ********************************************************************************************************** *
     *                                          Private utility methods                                           *
     * ********************************************************************************************************** */

    /**
     * Register command executors.
     */
    private fun registerCommandExecutors() {
        getCommand("worldborder")!!.setExecutor(injector.getInstance(BaseCommand::class.java))
    }
}
