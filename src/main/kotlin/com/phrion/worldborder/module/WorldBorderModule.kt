/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.module

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.phrion.worldborder.Logging
import com.phrion.worldborder.command.SetCommand
import com.phrion.worldborder.command.WorldBorderCommand
import java.io.File
import javax.inject.Named
import org.bukkit.plugin.Plugin
import org.slf4j.Logger
import javax.inject.Singleton

/**
 * WorldBorder plugin Guice module.
 *
 * @since 2021-12-13
 */
class WorldBorderModule(
    override val log: Logger,
    private val plugin: Plugin
) : AbstractModule(), Logging {

    /**
     * @return map of command names to [WorldBorderCommand]s.
     */
    @Provides
    @Singleton
    fun commands(
        setCommand: SetCommand
    ): Map<String, WorldBorderCommand> {
        return mapOf(
            "set" to setCommand
        )
    }

    @Named("dataFolder")
    @Provides
    @Singleton
    fun dataFolder(): File {
        return plugin.dataFolder
    }

    /**
     * @return plugin instance of SLF4J [Logger].
     */
    @Provides
    @Singleton
    fun logger(): Logger {
        return plugin.slF4JLogger
    }
}
