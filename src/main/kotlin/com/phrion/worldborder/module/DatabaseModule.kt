/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.module

import com.phrion.worldborder.data.Config
import com.phrion.worldborder.data.DatabaseSelection
import com.phrion.worldborder.data.MongoConfig
import com.phrion.worldborder.data.WorldBorder
import com.phrion.worldborder.provider.MySQLConnectionProvider
import com.phrion.worldborder.provider.SQLiteConnectionProvider
import com.phrion.worldborder.repository.WorldBorderFileRepository
import com.phrion.worldborder.repository.WorldBorderMongoRepository
import com.phrion.worldborder.repository.WorldBorderRepository
import com.phrion.worldborder.repository.WorldBorderSQLRepository
import java.io.File
import java.sql.Connection
import javax.inject.Named
import javax.inject.Singleton
import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.google.inject.multibindings.OptionalBinder
import com.mongodb.MongoClientSettings.getDefaultCodecRegistry
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries
import org.bson.codecs.configuration.CodecRegistry
import org.bson.codecs.pojo.PojoCodecProvider
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.configuration.file.YamlConfiguration


/**
 * Database Guice module.
 *
 * @since 2021-12-17
 */
class DatabaseModule(private val config: Config) : AbstractModule() {
    override fun configure() {
        // Database connections
        OptionalBinder.newOptionalBinder(binder(), Connection::class.java)
        OptionalBinder.newOptionalBinder(binder(), WorldBorderRepository::class.java)

        when (config.databaseConfig.databaseSelection) {
            DatabaseSelection.FLAT_FILE -> {
                OptionalBinder.newOptionalBinder(binder(), WorldBorderRepository::class.java)
                    .setBinding()
                    .to(WorldBorderFileRepository::class.java)
                    .`in`(Singleton::class.java)
            }
            DatabaseSelection.MONGODB -> {
                OptionalBinder.newOptionalBinder(binder(), WorldBorderRepository::class.java)
                    .setBinding()
                    .to(WorldBorderMongoRepository::class.java)
                    .`in`(Singleton::class.java)
            }
            DatabaseSelection.MYSQL -> {
                OptionalBinder.newOptionalBinder(binder(), Connection::class.java)
                    .setBinding()
                    .toProvider(MySQLConnectionProvider::class.java)
                    .`in`(Singleton::class.java)
                OptionalBinder.newOptionalBinder(binder(), WorldBorderRepository::class.java)
                    .setBinding()
                    .to(WorldBorderSQLRepository::class.java)
                    .`in`(Singleton::class.java)
            }
            DatabaseSelection.SQLITE -> {
                OptionalBinder.newOptionalBinder(binder(), Connection::class.java)
                    .setBinding()
                    .toProvider(SQLiteConnectionProvider::class.java)
                    .`in`(Singleton::class.java)
                OptionalBinder.newOptionalBinder(binder(), WorldBorderRepository::class.java)
                    .setBinding()
                    .to(WorldBorderSQLRepository::class.java)
                    .`in`(Singleton::class.java)
            }
        }
    }

    /**
     * @return MongoDB codec registry for automatic (de)serialization.
     */
    @Provides
    @Singleton
    fun codecRegistry(): CodecRegistry {
        val pojoCodecProvider: CodecProvider = PojoCodecProvider.builder().automatic(true).build()

        return fromRegistries(getDefaultCodecRegistry(), fromProviders(pojoCodecProvider))
    }

    /**
     * @return Mongo client.
     */
    @Provides
    @Singleton
    fun mongoClient(config: MongoConfig): MongoClient {
        val uri = "mongodb://${config.username}:${config.password}@${config.hostname}:${config.port}/${config.options}"

        return MongoClients.create(uri)
    }

    /**
     * @return Mongo database.
     */
    @Provides
    @Singleton
    fun mongoDatabase(client: MongoClient, codecRegistry: CodecRegistry, config: MongoConfig): MongoDatabase {
        return client.getDatabase(config.database).withCodecRegistry(codecRegistry)
    }

    /**
     * @return [WorldBorder] MongoDB collection.
     */
    @Provides
    @Singleton
    fun worldBorderCollection(
        database: MongoDatabase,
        @Named("tablePrefix") tablePrefix: String
    ): MongoCollection<WorldBorder> {
        return database.getCollection("${tablePrefix}worldBorder", WorldBorder::class.java)
    }

    /**
     * @return [File] for [WorldBorder] file storage.
     */
    @Named("worldBorderFile")
    @Provides
    @Singleton
    fun worldBorderFile(@Named("dataFolder") dataFolder: File): File {
        return File(dataFolder, "worldBorder.yml")
    }

    /**
     * @return YAML wrapper for [worldBorderFile].
     */
    @Named("worldBorderFileConfiguration")
    @Provides
    @Singleton
    fun worldBorderFileConfiguration(@Named("worldBorderFile") worldBorderFile: File): FileConfiguration {
        return YamlConfiguration.loadConfiguration(worldBorderFile)
    }
}
