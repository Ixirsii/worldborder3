/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.module

import com.google.inject.AbstractModule
import com.google.inject.Provides
import com.phrion.worldborder.Logging
import com.phrion.worldborder.data.*
import org.bukkit.configuration.file.FileConfiguration
import org.bukkit.plugin.Plugin
import org.slf4j.Logger
import javax.inject.Named
import javax.inject.Singleton

class ConfigModule(override val log: Logger, private val plugin: Plugin) : AbstractModule(), Logging {

    /**
     * @return plugin configuration.
     */
    @Provides
    @Singleton
    fun config(fileConfiguration: FileConfiguration, databaseConfig: DatabaseConfig): Config {
        return Config(
            checkRateInTicks = fileConfiguration.getInt(ConfigSchema.CHECK_RATE_IN_TICKS.key),
            databaseConfig = databaseConfig,
            defaultAutoSaveFrequency = fileConfiguration.getInt(ConfigSchema.AUTOSAVE_FREQUENCY.key),
            defaultBorderMode = BorderMode.valueOf(fileConfiguration.getString(ConfigSchema.BORDER_MODE.key)!!),
            defaultDenyEnderPearl = fileConfiguration.getBoolean(ConfigSchema.DENY_ENDER_PEARLS.key),
            defaultKnockBack = fileConfiguration.getDouble(ConfigSchema.KNOCK_BACK.key),
            defaultMessage = fileConfiguration.getString(ConfigSchema.MESSAGE.key)!!,
            defaultRoundBorder = fileConfiguration.getBoolean(ConfigSchema.ROUND_BORDER.key),
            defaultWhooshEffect = fileConfiguration.getBoolean(ConfigSchema.WHOOSH_EFFECT.key),
            dynmapEnabled = fileConfiguration.getBoolean(ConfigSchema.DYNMAP_ENABLED.key),
            dynmapLabel = fileConfiguration.getString(ConfigSchema.DYNMAP_LABEL.key)!!,
            taskMemoryUsageTolerance = fileConfiguration.getInt(ConfigSchema.TASK_MEMORY_USAGE_TOLERANCE.key),
        )
    }

    /**
     * @return database configuration.
     */
    @Provides
    @Singleton
    fun databaseConfig(
        fileConfiguration: FileConfiguration,
        mongoConfig: MongoConfig,
        mySQLConfig: MySQLConfig
    ): DatabaseConfig {
        val databaseMap: Map<DatabaseSelection, Boolean> = mapOf(
            DatabaseSelection.FLAT_FILE to fileConfiguration.getBoolean(ConfigSchema.FLAT_FILE_ENABLED.key),
            DatabaseSelection.MONGODB to fileConfiguration.getBoolean(ConfigSchema.MONGODB_ENABLED.key),
            DatabaseSelection.MYSQL to fileConfiguration.getBoolean(ConfigSchema.MYSQL_ENABLED.key),
            DatabaseSelection.SQLITE to fileConfiguration.getBoolean(ConfigSchema.SQLITE_ENABLED.key)
        )
        val enabledDatabase: DatabaseSelection = validateDatabaseSelection(databaseMap)

        return DatabaseConfig(
            databaseSelection = enabledDatabase,
            mongoConfig = mongoConfig,
            mySQLConfig = mySQLConfig
        )
    }

    /**
     * @return config.yml wrapper.
     */
    @Provides
    fun fileConfiguration(): FileConfiguration {
        return plugin.config
    }

    /**
     * @return MongoDB configuration.
     */
    @Provides
    @Singleton
    fun mongoConfig(fileConfiguration: FileConfiguration): MongoConfig {
        return MongoConfig(
            collectionPrefix = fileConfiguration.getString(ConfigSchema.MONGODB_PREFIX.key)!!,
            database = fileConfiguration.getString(ConfigSchema.MONGODB_DATABASE.key)!!,
            hostname = fileConfiguration.getString(ConfigSchema.MONGODB_HOSTNAME.key)!!,
            options = fileConfiguration.getString(ConfigSchema.MONGODB_OPTIONS.key)!!,
            password = fileConfiguration.getString(ConfigSchema.MONGODB_PASSWORD.key)!!,
            port = fileConfiguration.getInt(ConfigSchema.MONGODB_PORT.key),
            username = fileConfiguration.getString(ConfigSchema.MONGODB_USERNAME.key)!!
        )
    }

    /**
     * @return MySQL configuration.
     */
    @Provides
    @Singleton
    fun mysqlConfig(fileConfiguration: FileConfiguration): MySQLConfig {
        return MySQLConfig(
            database = fileConfiguration.getString(ConfigSchema.MYSQL_DATABASE.key)!!,
            options = fileConfiguration.getString(ConfigSchema.MYSQL_OPTIONS.key)!!,
            hostname = fileConfiguration.getString(ConfigSchema.MYSQL_HOSTNAME.key)!!,
            password = fileConfiguration.getString(ConfigSchema.MYSQL_PASSWORD.key)!!,
            port = fileConfiguration.getInt(ConfigSchema.MYSQL_PORT.key),
            tablePrefix = fileConfiguration.getString(ConfigSchema.MYSQL_PREFIX.key)!!,
            username = fileConfiguration.getString(ConfigSchema.MYSQL_USERNAME.key)!!
        )
    }

    /**
     * @return MySQL table prefix.
     */
    @Named("tablePrefix")
    @Provides
    @Singleton
    fun tablePrefix(databaseConfig: DatabaseConfig): String {
        return when (databaseConfig.databaseSelection) {
            DatabaseSelection.MONGODB -> databaseConfig.mongoConfig.collectionPrefix
            DatabaseSelection.MYSQL -> databaseConfig.mySQLConfig.tablePrefix
            else -> ""
        }
    }

    /* ********************************************************************************************************** *
     *                                          Private utility methods                                           *
     * ********************************************************************************************************** */

    /**
     * Validate enabled databases.
     *
     * If exactly one database is enabled in the config that database is returned.
     * If no databases are enabled SQLite is returned.
     * If more than 1 database is enabled then an enabled database is returned in order of priority:
     *  1. MongoDB
     *  2. MySQL
     *  3. SQLite
     *  4. Flat file
     *
     * @param databaseMap Map of databases to whether or not they're enabled.
     * @return enabled database or default.
     */
    private fun validateDatabaseSelection(databaseMap: Map<DatabaseSelection, Boolean>): DatabaseSelection {
        val enabledDatabaseCount: Int = databaseMap.count { (_, value) -> value }

        if (enabledDatabaseCount < 1) {
            log.error("No databases enabled. Defaulting to SQLite")

            return DatabaseSelection.SQLITE
        } else if (enabledDatabaseCount > 1) {
            val default: String
            val enabledDatabase: DatabaseSelection

            if (databaseMap[DatabaseSelection.MONGODB] == true) {
                enabledDatabase = DatabaseSelection.MONGODB
                default = "MongoDB"
            } else if (databaseMap[DatabaseSelection.MYSQL] == true) {
                enabledDatabase = DatabaseSelection.MYSQL
                default = "MySQL"
            } else {
                enabledDatabase = DatabaseSelection.SQLITE
                default = "SQLite"
            }

            log.error("Too many databases enabled. Defaulting to $default")

            return enabledDatabase
        } else {
            return databaseMap.firstNotNullOf { (key, value) -> if (value) key else null }
        }
    }
}