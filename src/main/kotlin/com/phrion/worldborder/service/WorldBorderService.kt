/******************************************************************************
 * Copyright (c) 2021, Ryan Porterfield, Steven Foskett                       *
 * All rights reserved.                                                       *
 *                                                                            *
 * Redistribution and use in source and binary forms, with or without         *
 * modification, are permitted provided that the following conditions         *
 * are met:                                                                   *
 *                                                                            *
 * 1. Redistributions of source code must retain the above copyright          *
 * notice, this list of conditions and the following disclaimer.              *
 *                                                                            *
 * 2. Redistributions in binary form must reproduce the above copyright       *
 * notice, this list of conditions and the following disclaimer in the        *
 * documentation and/or other materials provided with the distribution.       *
 *                                                                            *
 * 3. Neither the name of the copyright holder nor the names of its           *
 * contributors may be used to endorse or promote products derived from       *
 * this software without specific prior written permission.                   *
 *                                                                            *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS    *
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED      *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A            *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT         *
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,     *
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED   *
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR     *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF     *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING       *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               *
 ******************************************************************************/

package com.phrion.worldborder.service

import com.phrion.worldborder.Logging
import com.phrion.worldborder.data.WorldBorder
import com.phrion.worldborder.repository.WorldBorderMongoRepository
import com.phrion.worldborder.repository.WorldBorderRepository
import java.io.IOException
import java.sql.SQLException
import java.util.Optional
import javax.inject.Inject
import javax.inject.Singleton
import com.mongodb.MongoException
import org.slf4j.Logger

/**
 * Business logic wrapper for [WorldBorderMongoRepository].
 *
 * @since 2021-12-18
 */
@Singleton
class WorldBorderService @Inject constructor(
    /** Mongo repository for the [WorldBorder] collection. */
    private val repository: WorldBorderRepository,
    /** SLF4J plugin logger. */
    override val log: Logger
) : Logging {

    /**
     * Save a world border to the database.
     *
     * @param worldBorder World border to save to the database.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun addBorder(worldBorder: WorldBorder): Boolean {
        return try {
            repository.createBorder(worldBorder)
        } catch (e: IOException) {
            log.error("Caught exception attempting to add world border to file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to add world border to MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to add world border to SQL database", e)

            false
        }
    }

    /**
     * Get border information for a world from the database.
     *
     * @param world World name.
     * @return [WorldBorder] for world if it exists, otherwise `empty`.
     */
    fun getBorder(world: String): Optional<WorldBorder> {
        return try {
            repository.readBorder(world)
        } catch (e: IOException) {
            log.error("Caught exception attempting to get world border from file", e)

            Optional.empty()
        } catch (e: MongoException) {
            log.error("Caught exception attempting to get world border from MongoDB", e)

            Optional.empty()
        } catch (e: SQLException) {
            log.error("Caught exception attempting to get world border from SQL database", e)

            Optional.empty()
        }
    }

    /**
     * List all world borders from the database.
     *
     * @return all world borders from the database.
     */
    fun listBorders(): List<WorldBorder> {
        return try {
            repository.readBorders()
        } catch (e: IOException) {
            log.error("Caught exception attempting to list all world borders from file", e)

            emptyList()
        } catch (e: MongoException) {
            log.error("Caught exception attempting to list all world borders from MongoDB", e)

            emptyList()
        } catch (e: SQLException) {
            log.error("Caught exception attempting to list all world borders from SQL database", e)

            emptyList()
        }
    }

    /**
     * Delete border for a world from the database.
     *
     * @param world World name.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun removeBorder(world: String): Boolean {
        return try {
            repository.deleteBorder(world)
        } catch (e: IOException) {
            log.error("Caught exception attempting to delete world border from file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to delete world border from MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to delete world border from SQL database", e)

            false
        }
    }

    /**
     * Update world border center point.
     *
     * @param world World name.
     * @param x Center point x-coordinate.
     * @param z Center point z-coordinate.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateCenter(world: String, x: Double, z: Double): Boolean {
        return try {
            repository.updateCenter(world, x, z)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update center point value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update center point value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update center point value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border ender pearl behavior.
     *
     * @param world World name.
     * @param denyEnderPearl Ender pearl behavior.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateDenyEnderPearl(world: String, denyEnderPearl: Boolean): Boolean {
        return try {
            repository.updateDenyEnderPearl(world, denyEnderPearl)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update denyEnderPearl value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update denyEnderPearl value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update denyEnderPearl value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border knock back distance.
     *
     * @param world World name.
     * @param knockBack Knock back distance.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateKnockBack(world: String, knockBack: Double): Boolean {
        return try {
            repository.updateKnockBack(world, knockBack)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update knockBack value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update knockBack value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update knockBack value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border message.
     *
     * @param world World name.
     * @param message Message sent to players who cross the border.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateMessage(world: String, message: String): Boolean {
        return try {
            repository.updateMessage(world, message)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update message value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update message value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update message value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border radius.
     *
     * @param world World name.
     * @param radiusX Border radius along the x-axis.
     * @param radiusZ Border radius along the z-axis.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateRadius(world: String, radiusX: Int, radiusZ: Int): Boolean {
        return try {
            repository.updateRadius(world, radiusX, radiusZ)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update radius value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update radius value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update radius value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border shape.
     *
     * @param world World name.
     * @param roundBorder Border shape.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateRoundBorder(world: String, roundBorder: Boolean): Boolean {
        return try {
            repository.updateRoundBorder(world, roundBorder)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update roundBorder value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update roundBorder value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update roundBorder value of world border in SQL database", e)

            false
        }
    }

    /**
     * Update border whoosh effect.
     *
     * @param world World name.
     * @param whooshEffect Does crossing the border play a "whoosh" sound effect.
     * @return `true` if operation completed successfully, otherwise `false`.
     */
    fun updateWhooshEffect(world: String, whooshEffect: Boolean): Boolean {
        return try {
            repository.updateWhooshEffect(world, whooshEffect)
        } catch (e: IOException) {
            log.error("Caught exception attempting to update whooshEffect value of world border in file", e)

            false
        } catch (e: MongoException) {
            log.error("Caught exception attempting to update whooshEffect value of world border in MongoDB", e)

            false
        } catch (e: SQLException) {
            log.error("Caught exception attempting to update whooshEffect value of world border in SQL database", e)

            false
        }
    }
}
