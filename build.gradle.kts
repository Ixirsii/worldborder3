plugins {
    kotlin("jvm") version "1.5.10"
    idea
    java
}

group = "org.phrion.worldborder"
version = "3.0.0-SNAPSHOT"

repositories {
    mavenCentral()
    // Dynnmap
    maven("https://repo.mikeprimm.com/")
    // OSS required for Spigot API
    maven("https://oss.sonatype.org/content/repositories/snapshots")
    maven("https://oss.sonatype.org/content/repositories/central")
    // Paper
    maven("https://papermc.io/repo/repository/maven-public/")
    // Spigot API
    maven("https://hub.spigotmc.org/nexus/content/repositories/snapshots/")
}

dependencies {
    annotationProcessor("org.projectlombok:lombok:1.18.22")
    
    compileOnly("io.papermc.paper:paper-api:1.18.1-R0.1-SNAPSHOT")
    compileOnly("org.projectlombok:lombok:1.18.22")
    compileOnly("org.spigotmc:spigot-api:1.18-R0.1-SNAPSHOT")

    implementation(kotlin("stdlib-jdk8"))
    implementation("com.google.inject:guice:5.0.1")
    implementation("org.mongodb:mongodb-driver-sync:4.4.0")

    testImplementation("org.junit.jupiter:junit-jupiter-api:5.8.2")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine")
}

tasks.getByName<Test>("test") {
    useJUnitPlatform()
}
